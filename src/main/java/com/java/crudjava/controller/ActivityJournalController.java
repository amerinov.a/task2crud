package com.java.crudjava.controller;

import com.java.crudjava.entity.ActivityJournal;
import com.java.crudjava.service.ActivityJournalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActivityJournalController {
    private final ActivityJournalService activityJournalService;

    public ActivityJournalController(ActivityJournalService activityJournalService){
        this.activityJournalService = activityJournalService;
    }

    @GetMapping("/activityjournals/{activityJournalId}")
    public ResponseEntity<?> getActivityJournal(@PathVariable Long activityJournalId){
        return ResponseEntity.ok(activityJournalService.getById(activityJournalId));
    }

    @GetMapping("/activityjournals")
    public ResponseEntity<?> getActivityJournal(){
        return ResponseEntity.ok(activityJournalService.getAll());
    }

    @PostMapping("/activityjournals")
    public ResponseEntity<?> saveActivityJournal(@RequestBody ActivityJournal activityJournal) {
        return ResponseEntity.ok(activityJournalService.create(activityJournal));
    }

    @PutMapping("/activityjournals")
    public ResponseEntity<?> updateAuthorization(@RequestBody ActivityJournal activityJournal) {
        return ResponseEntity.ok(activityJournalService.update(activityJournal));
    }

    @DeleteMapping("/activityjournals/{activityJournalId}")
    public void deleteAuthorization(@PathVariable Long activityJournalId) {
        activityJournalService.delete(activityJournalId);
    }
}
