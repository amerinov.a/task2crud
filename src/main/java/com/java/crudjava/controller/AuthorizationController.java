package com.java.crudjava.controller;

import com.java.crudjava.entity.Authorization;
import com.java.crudjava.service.AuthorizationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthorizationController {
    private final AuthorizationService authorizationService;

    public AuthorizationController(AuthorizationService authorizationService){
        this.authorizationService = authorizationService;
    }

    @GetMapping("/authorizations/{authorizationId}")
    public ResponseEntity<?> getAuthorization(@PathVariable Long authorizationId){
        return ResponseEntity.ok(authorizationService.getById(authorizationId));
    }

    @GetMapping("/authorizations")
    public ResponseEntity<?> getAuthorization(){
        return ResponseEntity.ok(authorizationService.getAll());
    }

    @PostMapping("/authorizations")
    public ResponseEntity<?> saveAuthorization(@RequestBody Authorization authorization) {
        return ResponseEntity.ok(authorizationService.create(authorization));
    }

    @PutMapping("/authorizations")
    public ResponseEntity<?> updateAuthorization(@RequestBody Authorization authorization) {
        return ResponseEntity.ok(authorizationService.update(authorization));
    }

    @DeleteMapping("/authorizations/{authorizationId}")
    public void deleteAuthorization(@PathVariable Long authorizationId) {
        authorizationService.delete(authorizationId);
    }
}