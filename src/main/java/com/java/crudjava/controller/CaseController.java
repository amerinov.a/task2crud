package com.java.crudjava.controller;

import com.java.crudjava.entity.Case;
import com.java.crudjava.service.CaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseController {
    private final CaseService caseService;

    public CaseController(CaseService caseService){
        this.caseService = caseService;
    }

    @GetMapping("case/{caseId}")
    public ResponseEntity<?> getCase(@PathVariable Long caseId){
        return ResponseEntity.ok(caseService.getById(caseId));
    }

    @GetMapping("case")
    public ResponseEntity<?> getCase(){
        return ResponseEntity.ok(caseService.getAll());
    }

    @PostMapping("case")
    public ResponseEntity<?> saveCase(@RequestBody Case caze) {
        return ResponseEntity.ok(caseService.create(caze));
    }

    @PutMapping("case")
    public ResponseEntity<?> updateCase(@RequestBody Case caze) {
        return ResponseEntity.ok(caseService.update(caze));
    }

    @DeleteMapping("case/{caseId}")
    public void deleteCase(@PathVariable Long caseId) {
        caseService.delete(caseId);
    }
}
