package com.java.crudjava.controller;

import com.java.crudjava.entity.CaseIndex;
import com.java.crudjava.service.CaseIndexService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseIndexController {
    private final CaseIndexService caseIndexService;

    public CaseIndexController(CaseIndexService caseIndexService){
        this.caseIndexService = caseIndexService;
    }

    @GetMapping("caseindex/{caseIndexId}")
    public ResponseEntity<?> getCaseIndex(@PathVariable Long caseIndexId){
        return ResponseEntity.ok(caseIndexService.getById(caseIndexId));
    }

    @GetMapping("caseindex")
    public ResponseEntity<?> getCaseIndex(){
        return ResponseEntity.ok(caseIndexService.getAll());
    }

    @PostMapping("caseindex")
    public ResponseEntity<?> saveCaseIndex(@RequestBody CaseIndex caseIndex) {
        return ResponseEntity.ok(caseIndexService.create(caseIndex));
    }

    @PutMapping("caseindex")
    public ResponseEntity<?> updateCaseIndex(@RequestBody CaseIndex caseIndex) {
        return ResponseEntity.ok(caseIndexService.update(caseIndex));
    }

    @DeleteMapping("caseindex/{caseIndexId}")
    public void deleteCaseIndex(@PathVariable Long caseIndexId) {
        caseIndexService.delete(caseIndexId);
    }
}
