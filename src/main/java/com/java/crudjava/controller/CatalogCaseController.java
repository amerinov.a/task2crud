package com.java.crudjava.controller;

import com.java.crudjava.entity.CatalogCase;
import com.java.crudjava.service.CatalogCaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogCaseController {
    private final CatalogCaseService catalogCaseService;

    public CatalogCaseController(CatalogCaseService catalogCaseService){
        this.catalogCaseService = catalogCaseService;
    }

    @GetMapping("catalogCase/{catalogCaseId}")
    public ResponseEntity<?> getCatalogCase(@PathVariable Long catalogCaseId){
        return ResponseEntity.ok(catalogCaseService.getById(catalogCaseId));
    }

    @GetMapping("catalogCase")
    public ResponseEntity<?> getCatalogCase(){
        return ResponseEntity.ok(catalogCaseService.getAll());
    }

    @PostMapping("catalogCase")
    public ResponseEntity<?> saveCatalogCase(@RequestBody CatalogCase catalogCase) {
        return ResponseEntity.ok(catalogCaseService.create(catalogCase));
    }

    @PutMapping("catalogCase")
    public ResponseEntity<?> updateCatalogCase(@RequestBody CatalogCase catalogCase) {
        return ResponseEntity.ok(catalogCaseService.update(catalogCase));
    }

    @DeleteMapping("catalogCase/{catalogCaseId}")
    public void deleteCatalogCase(@PathVariable Long catalogCaseId) {
        catalogCaseService.delete(catalogCaseId);
    }
}
