package com.java.crudjava.controller;

import com.java.crudjava.entity.Catalog;
import com.java.crudjava.service.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogController {
    private final CatalogService catalogService;

    public CatalogController(CatalogService catalogService){
        this.catalogService = catalogService;
    }

    @GetMapping("catalog/{catalogId}")
    public ResponseEntity<?> getCatalog(@PathVariable Long catalogId){
        return ResponseEntity.ok(catalogService.getById(catalogId));
    }

    @GetMapping("catalog")
    public ResponseEntity<?> getCatalog(){
        return ResponseEntity.ok(catalogService.getAll());
    }

    @PostMapping("catalog")
    public ResponseEntity<?> saveCatalog(@RequestBody Catalog catalog) {
        return ResponseEntity.ok(catalogService.create(catalog));
    }

    @PutMapping("catalog")
    public ResponseEntity<?> updateCatalog(@RequestBody Catalog catalog) {
        return ResponseEntity.ok(catalogService.update(catalog));
    }

    @DeleteMapping("catalog/{catalogId}")
    public void deleteCatalog(@PathVariable Long catalogId) {
        catalogService.delete(catalogId);
    }
}
