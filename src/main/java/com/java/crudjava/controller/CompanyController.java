package com.java.crudjava.controller;

import com.java.crudjava.entity.Company;
import com.java.crudjava.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyController {
    private final CompanyService companyService;

    public CompanyController(CompanyService companyService){
        this.companyService = companyService;
    }

    @GetMapping("company/{companyId}")
    public ResponseEntity<?> getCompany(@PathVariable Long companyId){
        return ResponseEntity.ok(companyService.getById(companyId));
    }

    @GetMapping("company")
    public ResponseEntity<?> getCompany(){
        return ResponseEntity.ok(companyService.getAll());
    }

    @PostMapping("company")
    public ResponseEntity<?> saveCompany(@RequestBody Company company) {
        return ResponseEntity.ok(companyService.create(company));
    }

    @PutMapping("company")
    public ResponseEntity<?> updateCompany(@RequestBody Company company) {
        return ResponseEntity.ok(companyService.update(company));
    }

    @DeleteMapping("company/{companyId}")
    public void deleteCompany(@PathVariable Long companyId) {
        companyService.delete(companyId);
    }
}
