package com.java.crudjava.controller;

import com.java.crudjava.entity.CompanyUnit;
import com.java.crudjava.service.CompanyUnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyUnitController {
    private final CompanyUnitService companyUnitService;

    public CompanyUnitController(CompanyUnitService companyUnitService){
        this.companyUnitService = companyUnitService;
    }

    @GetMapping("companyunit/{companyUnitId}")
    public ResponseEntity<?> getCompanyUnit(@PathVariable Long companyUnitId){
        return ResponseEntity.ok(companyUnitService.getById(companyUnitId));
    }

    @GetMapping("companyunit")
    public ResponseEntity<?> getCompanyUnit(){
        return ResponseEntity.ok(companyUnitService.getAll());
    }

    @PostMapping("companyunit")
    public ResponseEntity<?> saveCompanyUnit(@RequestBody CompanyUnit companyUnit) {
        return ResponseEntity.ok(companyUnitService.create(companyUnit));
    }

    @PutMapping("companyunit")
    public ResponseEntity<?> updateCompanyUnit(@RequestBody CompanyUnit companyUnit) {
        return ResponseEntity.ok(companyUnitService.update(companyUnit));
    }

    @DeleteMapping("companyunit/{companyUnitId}")
    public void deleteCompanyUnit(@PathVariable Long companyUnitId) {
        companyUnitService.delete(companyUnitId);
    }
}
