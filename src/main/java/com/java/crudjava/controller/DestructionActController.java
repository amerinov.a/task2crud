package com.java.crudjava.controller;

import com.java.crudjava.entity.DestructionAct;
import com.java.crudjava.service.DestructionActService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DestructionActController {
    private final DestructionActService destructionActService;

    public DestructionActController(DestructionActService destructionActService){
        this.destructionActService = destructionActService;
    }

    @GetMapping("destructionact/{destructionActId}")
    public ResponseEntity<?> getDestructionAct(@PathVariable Long destructionActId){
        return ResponseEntity.ok(destructionActService.getById(destructionActId));
    }

    @GetMapping("destructionact")
    public ResponseEntity<?> getDestructionAct(){
        return ResponseEntity.ok(destructionActService.getAll());
    }

    @PostMapping("destructionact")
    public ResponseEntity<?> saveDestructionAct(@RequestBody DestructionAct destructionAct) {
        return ResponseEntity.ok(destructionActService.create(destructionAct));
    }

    @PutMapping("destructionact")
    public ResponseEntity<?> updateDestructionAct(@RequestBody DestructionAct destructionAct) {
        return ResponseEntity.ok(destructionActService.update(destructionAct));
    }

    @DeleteMapping("destructionact/{destructionActId}")
    public void deleteDestructionAct(@PathVariable Long destructionActId) {
        destructionActService.delete(destructionActId);
    }
}
