package com.java.crudjava.controller;

import com.java.crudjava.entity.File;
import com.java.crudjava.service.FileService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileController {
    private final FileService fileService;

    public FileController(FileService fileService){
        this.fileService = fileService;
    }

    @GetMapping("file/{fileId}")
    public ResponseEntity<?> getFile(@PathVariable Long fileId){
        return ResponseEntity.ok(fileService.getById(fileId));
    }

    @GetMapping("file")
    public ResponseEntity<?> getFile(){
        return ResponseEntity.ok(fileService.getAll());
    }

    @PostMapping("file")
    public ResponseEntity<?> saveFile(@RequestBody File file) {
        return ResponseEntity.ok(fileService.create(file));
    }

    @PutMapping("file")
    public ResponseEntity<?> updateFile(@RequestBody File file) {
        return ResponseEntity.ok(fileService.update(file));
    }

    @DeleteMapping("file/{fileId}")
    public void deleteFile(@PathVariable Long fileId) {
        fileService.delete(fileId);
    }
}
