package com.java.crudjava.controller;

import com.java.crudjava.entity.Fond;
import com.java.crudjava.service.FondService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FondController {
    private final FondService fondService;

    public FondController(FondService fondService){
        this.fondService = fondService;
    }

    @GetMapping("fond/{fondId}")
    public ResponseEntity<?> getFond(@PathVariable Long fondId){
        return ResponseEntity.ok(fondService.getById(fondId));
    }

    @GetMapping("fond")
    public ResponseEntity<?> getFond(){
        return ResponseEntity.ok(fondService.getAll());
    }

    @PostMapping("fond")
    public ResponseEntity<?> saveFond(@RequestBody Fond fond) {
        return ResponseEntity.ok(fondService.create(fond));
    }

    @PutMapping("fond")
    public ResponseEntity<?> updateFond(@RequestBody Fond fond) {
        return ResponseEntity.ok(fondService.update(fond));
    }

    @DeleteMapping("fond/{fondId}")
    public void deleteFond(@PathVariable Long fondId) {
        fondService.delete(fondId);
    }
}
