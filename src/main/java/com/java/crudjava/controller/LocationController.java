package com.java.crudjava.controller;

import com.java.crudjava.entity.Location;
import com.java.crudjava.service.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LocationController {
    private final LocationService locationService;

    public LocationController(LocationService locationService){
        this.locationService = locationService;
    }

    @GetMapping("location/{locationId}")
    public ResponseEntity<?> getLocation(@PathVariable Long locationId){
        return ResponseEntity.ok(locationService.getById(locationId));
    }

    @GetMapping("location")
    public ResponseEntity<?> getLocation(){
        return ResponseEntity.ok(locationService.getAll());
    }

    @PostMapping("location")
    public ResponseEntity<?> saveLocation(@RequestBody Location location) {
        return ResponseEntity.ok(locationService.create(location));
    }

    @PutMapping("location")
    public ResponseEntity<?> updateLocation(@RequestBody Location location) {
        return ResponseEntity.ok(locationService.update(location));
    }

    @DeleteMapping("location/{locationId}")
    public void deleteLocation(@PathVariable Long locationId) {
        locationService.delete(locationId);
    }
}
