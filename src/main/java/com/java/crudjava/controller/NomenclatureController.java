package com.java.crudjava.controller;

import com.java.crudjava.entity.Nomenclature;
import com.java.crudjava.service.NomenclatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureController {
    private final NomenclatureService nomenclatureService;

    public NomenclatureController(NomenclatureService nomenclatureService){
        this.nomenclatureService = nomenclatureService;
    }

    @GetMapping("nomenclature/{nomenclatureId}")
    public ResponseEntity<?> getNomenclature(@PathVariable Long nomenclatureId){
        return ResponseEntity.ok(nomenclatureService.getById(nomenclatureId));
    }

    @GetMapping("nomenclature")
    public ResponseEntity<?> getNomenclature(){
        return ResponseEntity.ok(nomenclatureService.getAll());
    }

    @PostMapping("nomenclature")
    public ResponseEntity<?> saveNomenclature(@RequestBody Nomenclature nomenclature) {
        return ResponseEntity.ok(nomenclatureService.create(nomenclature));
    }

    @PutMapping("nomenclature")
    public ResponseEntity<?> updateNomenclature(@RequestBody Nomenclature nomenclature) {
        return ResponseEntity.ok(nomenclatureService.update(nomenclature));
    }

    @DeleteMapping("nomenclature/{nomenclatureId}")
    public void deleteNomenclature(@PathVariable Long nomenclatureId) {
        nomenclatureService.delete(nomenclatureId);
    }
}
