package com.java.crudjava.controller;

import com.java.crudjava.entity.NomenclatureSummary;
import com.java.crudjava.service.NomenclatureSummaryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureSummaryController {
    private final NomenclatureSummaryService nomenclatureSummaryService;

    public NomenclatureSummaryController(NomenclatureSummaryService nomenclatureSummaryService){
        this.nomenclatureSummaryService = nomenclatureSummaryService;
    }

    @GetMapping("nomenclaturesummary/{nomenclatureSummaryId}")
    public ResponseEntity<?> getNomenclatureSummary(@PathVariable Long nomenclatureSummaryId){
        return ResponseEntity.ok(nomenclatureSummaryService.getById(nomenclatureSummaryId));
    }

    @GetMapping("nomenclaturesummary")
    public ResponseEntity<?> getNomenclatureSummary(){
        return ResponseEntity.ok(nomenclatureSummaryService.getAll());
    }

    @PostMapping("nomenclaturesummary")
    public ResponseEntity<?> saveNomenclatureSummary(@RequestBody NomenclatureSummary nomenclatureSummary) {
        return ResponseEntity.ok(nomenclatureSummaryService.create(nomenclatureSummary));
    }

    @PutMapping("nomenclaturesummary")
    public ResponseEntity<?> updateNomenclatureSummary(@RequestBody NomenclatureSummary nomenclatureSummary) {
        return ResponseEntity.ok(nomenclatureSummaryService.update(nomenclatureSummary));
    }

    @DeleteMapping("nomenclaturesummary/{nomenclatureSummaryId}")
    public void deleteNomenclatureSummary(@PathVariable Long nomenclatureSummaryId) {
        nomenclatureSummaryService.delete(nomenclatureSummaryId);
    }
}
