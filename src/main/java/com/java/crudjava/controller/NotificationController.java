package com.java.crudjava.controller;

import com.java.crudjava.entity.Notification;
import com.java.crudjava.service.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NotificationController {
    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService){
        this.notificationService = notificationService;
    }

    @GetMapping("notification/{notificationId}")
    public ResponseEntity<?> getNotification(@PathVariable Long notificationId){
        return ResponseEntity.ok(notificationService.getById(notificationId));
    }

    @GetMapping("notification")
    public ResponseEntity<?> getNotification(){
        return ResponseEntity.ok(notificationService.getAll());
    }

    @PostMapping("notification")
    public ResponseEntity<?> saveNotification(@RequestBody Notification notification) {
        return ResponseEntity.ok(notificationService.create(notification));
    }

    @PutMapping("notification")
    public ResponseEntity<?> updateNotification(@RequestBody Notification notification) {
        return ResponseEntity.ok(notificationService.update(notification));
    }

    @DeleteMapping("notification/{notificationId}")
    public void deleteNotification(@PathVariable Long notificationId) {
        notificationService.delete(notificationId);
    }
}
