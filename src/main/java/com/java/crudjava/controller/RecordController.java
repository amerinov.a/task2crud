package com.java.crudjava.controller;

import com.java.crudjava.entity.Record;
import com.java.crudjava.service.RecordService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RecordController {
    private final RecordService recordService;

    public RecordController(RecordService recordService){
        this.recordService = recordService;
    }

    @GetMapping("record/{recordId}")
    public ResponseEntity<?> getRecord(@PathVariable Long recordId){
        return ResponseEntity.ok(recordService.getById(recordId));
    }

    @GetMapping("record")
    public ResponseEntity<?> getRecord(){
        return ResponseEntity.ok(recordService.getAll());
    }

    @PostMapping("record")
    public ResponseEntity<?> saveRecord(@RequestBody Record record) {
        return ResponseEntity.ok(recordService.create(record));
    }

    @PutMapping("record")
    public ResponseEntity<?> updateRecord(@RequestBody Record record) {
        return ResponseEntity.ok(recordService.update(record));
    }

    @DeleteMapping("record/{recordId}")
    public void deleteRecord(@PathVariable Long recordId) {
        recordService.delete(recordId);
    }
}
