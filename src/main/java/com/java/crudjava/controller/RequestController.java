package com.java.crudjava.controller;

import com.java.crudjava.entity.Request;
import com.java.crudjava.service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestController {
    private final RequestService requestService;

    public RequestController(RequestService requestService){
        this.requestService = requestService;
    }

    @GetMapping("request/{requestId}")
    public ResponseEntity<?> getRequest(@PathVariable Long requestId){
        return ResponseEntity.ok(requestService.getById(requestId));
    }

    @GetMapping("request")
    public ResponseEntity<?> getRequest(){
        return ResponseEntity.ok(requestService.getAll());
    }

    @PostMapping("request")
    public ResponseEntity<?> saveRequest(@RequestBody Request request) {
        return ResponseEntity.ok(requestService.create(request));
    }

    @PutMapping("request")
    public ResponseEntity<?> updateRequest(@RequestBody Request request) {
        return ResponseEntity.ok(requestService.update(request));
    }

    @DeleteMapping("request/{requestId}")
    public void deleteRequest(@PathVariable Long requestId) {
        requestService.delete(requestId);
    }
}
