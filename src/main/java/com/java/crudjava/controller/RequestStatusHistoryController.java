package com.java.crudjava.controller;

import com.java.crudjava.entity.RequestStatusHistory;
import com.java.crudjava.service.RequestStatusHistoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestStatusHistoryController {
    private final RequestStatusHistoryService requestStatusHistoryService;

    public RequestStatusHistoryController(RequestStatusHistoryService requestStatusHistoryService){
        this.requestStatusHistoryService = requestStatusHistoryService;
    }

    @GetMapping("requeststatushistory/{requestStatusHistoryId}")
    public ResponseEntity<?> getRequestStatusHistory(@PathVariable Long requestStatusHistoryId){
        return ResponseEntity.ok(requestStatusHistoryService.getById(requestStatusHistoryId));
    }

    @GetMapping("requeststatushistory")
    public ResponseEntity<?> getRequestStatusHistory(){
        return ResponseEntity.ok(requestStatusHistoryService.getAll());
    }

    @PostMapping("requeststatushistory")
    public ResponseEntity<?> saveRequestStatusHistory(@RequestBody RequestStatusHistory requestStatusHistory) {
        return ResponseEntity.ok(requestStatusHistoryService.create(requestStatusHistory));
    }

    @PutMapping("requeststatushistory")
    public ResponseEntity<?> updateRequestStatusHistory(@RequestBody RequestStatusHistory requestStatusHistory) {
        return ResponseEntity.ok(requestStatusHistoryService.update(requestStatusHistory));
    }

    @DeleteMapping("requeststatushistory/{requestStatusHistoryId}")
    public void deleteRequestStatusHistory(@PathVariable Long requestStatusHistoryId) {
        requestStatusHistoryService.delete(requestStatusHistoryId);
    }
}
