package com.java.crudjava.controller;

import com.java.crudjava.entity.SearchKey;
import com.java.crudjava.service.SearchKeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyController {
    private final SearchKeyService searchKeyService;

    public SearchKeyController(SearchKeyService searchKeyService){
        this.searchKeyService = searchKeyService;
    }

    @GetMapping("searchkey/{searchKeyId}")
    public ResponseEntity<?> getSearchKey(@PathVariable Long searchKeyId){
        return ResponseEntity.ok(searchKeyService.getById(searchKeyId));
    }

    @GetMapping("searchkey")
    public ResponseEntity<?> getSearchKey(){
        return ResponseEntity.ok(searchKeyService.getAll());
    }

    @PostMapping("searchkey")
    public ResponseEntity<?> saveSearchKey(@RequestBody SearchKey searchKey) {
        return ResponseEntity.ok(searchKeyService.create(searchKey));
    }

    @PutMapping("searchkey")
    public ResponseEntity<?> updateSearchKey(@RequestBody SearchKey searchKey) {
        return ResponseEntity.ok(searchKeyService.update(searchKey));
    }

    @DeleteMapping("searchkey/{searchKeyId}")
    public void deleteSearchKey(@PathVariable Long searchKeyId) {
        searchKeyService.delete(searchKeyId);
    }
}
