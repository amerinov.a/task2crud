package com.java.crudjava.controller;

import com.java.crudjava.entity.SearchKeyRouting;
import com.java.crudjava.service.SearchKeyRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyRoutingController {
    private final SearchKeyRoutingService searchKeyRoutingService;

    public SearchKeyRoutingController(SearchKeyRoutingService searchKeyRoutingService){
        this.searchKeyRoutingService = searchKeyRoutingService;
    }

    @GetMapping("searchkeyrouting/{searchKeyRoutingId}")
    public ResponseEntity<?> getSearchKeyRouting(@PathVariable Long searchKeyRoutingId){
        return ResponseEntity.ok(searchKeyRoutingService.getById(searchKeyRoutingId));
    }

    @GetMapping("searchkeyrouting")
    public ResponseEntity<?> getSearchKeyRouting(){
        return ResponseEntity.ok(searchKeyRoutingService.getAll());
    }

    @PostMapping("searchkeyrouting")
    public ResponseEntity<?> saveSearchKeyRouting(@RequestBody SearchKeyRouting searchKeyRouting) {
        return ResponseEntity.ok(searchKeyRoutingService.create(searchKeyRouting));
    }

    @PutMapping("searchkeyrouting")
    public ResponseEntity<?> updateSearchKeyRouting(@RequestBody SearchKeyRouting searchKeyRouting) {
        return ResponseEntity.ok(searchKeyRoutingService.update(searchKeyRouting));
    }

    @DeleteMapping("searchkeyrouting/{searchKeyRoutingId}")
    public void deleteSearchKeyRouting(@PathVariable Long searchKeyRoutingId) {
        searchKeyRoutingService.delete(searchKeyRoutingId);
    }
}
