package com.java.crudjava.controller;

import com.java.crudjava.entity.Share;
import com.java.crudjava.service.ShareService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShareController {
    private final ShareService shareService;

    public ShareController(ShareService shareService){
        this.shareService = shareService;
    }

    @GetMapping("share/{shareId}")
    public ResponseEntity<?> getShare(@PathVariable Long shareId){
        return ResponseEntity.ok(shareService.getById(shareId));
    }

    @GetMapping("share")
    public ResponseEntity<?> getShare(){
        return ResponseEntity.ok(shareService.getAll());
    }

    @PostMapping("share")
    public ResponseEntity<?> saveShare(@RequestBody Share share) {
        return ResponseEntity.ok(shareService.create(share));
    }

    @PutMapping("share")
    public ResponseEntity<?> updateShare(@RequestBody Share share) {
        return ResponseEntity.ok(shareService.update(share));
    }

    @DeleteMapping("share/{shareId}")
    public void deleteShare(@PathVariable Long shareId) {
        shareService.delete(shareId);
    }
}
