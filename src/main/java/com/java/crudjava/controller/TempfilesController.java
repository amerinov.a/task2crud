package com.java.crudjava.controller;


import com.java.crudjava.entity.Tempfiles;
import com.java.crudjava.service.TempfilesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TempfilesController {
    private final TempfilesService tempfilesService;

    public TempfilesController(TempfilesService tempfilesService){
        this.tempfilesService = tempfilesService;
    }

    @GetMapping("tempfiles/{tempfilesId}")
    public ResponseEntity<?> getTempfiles(@PathVariable Long tempfilesId){
        return ResponseEntity.ok(tempfilesService.getById(tempfilesId));
    }

    @GetMapping("tempfiles")
    public ResponseEntity<?> getTempfiles(){
        return ResponseEntity.ok(tempfilesService.getAll());
    }

    @PostMapping("tempfiles")
    public ResponseEntity<?> saveTempfiles(@RequestBody Tempfiles tempfiles) {
        return ResponseEntity.ok(tempfilesService.create(tempfiles));
    }

    @PutMapping("tempfiles")
    public ResponseEntity<?> updateTempfiles(@RequestBody Tempfiles tempfiles) {
        return ResponseEntity.ok(tempfilesService.update(tempfiles));
    }

    @DeleteMapping("tempfiles/{tempfilesId}")
    public void deleteTempfiles(@PathVariable Long tempfilesId) {
        tempfilesService.delete(tempfilesId);
    }
}
