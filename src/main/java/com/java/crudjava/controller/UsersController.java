package com.java.crudjava.controller;

import com.java.crudjava.entity.Users;
import com.java.crudjava.service.UsersService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsersController {
    private final UsersService usersService;

    public UsersController(UsersService usersService){
        this.usersService = usersService;
    }

    @GetMapping("users/{usersId}")
    public ResponseEntity<?> getUsers(@PathVariable Long usersId){
        return ResponseEntity.ok(usersService.getById(usersId));
    }

    @GetMapping("users")
    public ResponseEntity<?> getUsers(){
        return ResponseEntity.ok(usersService.getAll());
    }

    @PostMapping("users")
    public ResponseEntity<?> saveUsers(@RequestBody Users users) {
        return ResponseEntity.ok(usersService.create(users));
    }

    @PutMapping("users")
    public ResponseEntity<?> updateUsers(@RequestBody Users users) {
        return ResponseEntity.ok(usersService.update(users));
    }

    @DeleteMapping("users/{usersId}")
    public void deleteUsers(@PathVariable Long usersId) {
        usersService.delete(usersId);
    }
}
