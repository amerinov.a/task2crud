package com.java.crudjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class crudjava {
    public static void main(String[] args) {
        SpringApplication.run(crudjava.class, args);
    }
}
