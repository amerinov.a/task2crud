package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "actjournal")
public class ActivityJournal {
    @Id
    @Column (name = "activityjournalid")
    private long activityJournalId;

    @Column (name = "activitytype")
    private String activityType;

    @Column (name = "objecttype")
    private String objectType;

    @Column (name = "objectid")
    private long objectId;

    @Column (name = "createdtimestamp")
    private long createdTimestamp;

    @Column (name = "createdby")
    private long createdBy;

    @Column (name = "messagelevel")
    private String messageLevel;

    @Column (name = "message")
    private String message;
}

