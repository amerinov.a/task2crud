package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "auth")
public class Authorization {
    @Id
    @Column (name = "authorizationid")
    private long authorizationId;

    @Column (name = "username")
    private String username;

    @Column (name = "email")
    private String email;

    @Column (name = "password")
    private String password;

    @Column (name = "role")
    private String role;

    @Column (name = "forgotpasswordkey")
    private String forgotPasswordKey;

    @Column (name = "forgotpasswordkeytimestamp")
    private long forgotPasswordKeyTimestamp;

    @Column (name = "companyunitid")
    private long companyUnitId;
}
