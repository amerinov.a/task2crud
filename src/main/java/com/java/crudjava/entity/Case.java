package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "case")
public class Case {
    @Id
    @Column (name = "caseid")
    private long caseId;

    @Column (name = "casenumber")
    private String caseNumber;

    @Column (name = "volumenumber")
    private String volumeNumber;

    @Column (name = "caseru")
    private String caseRu;

    @Column (name = "casekz")
    private String caseKz;

    @Column (name = "caseen")
    private String caseEn;

    @Column (name = "startdate")
    private long startDate;

    @Column (name = "finishdate")
    private long finishDate;

    @Column (name = "pagecount")
    private long pageCount;

    @Column (name = "isecpsignature")
    private boolean isEcpSignature;

    @Column (name = "ecpsignature")
    private String ecpSignature;

    @Column (name = "issentnaf")
    private boolean isSentNaf;

    @Column (name = "isdeleted")
    private boolean isDeleted;

    @Column (name = "isaccessrestricted")
    private boolean isAccessRestricted;

    @Column (name = "hash")
    private String hash;

    @Column (name = "version")
    private Integer version;

    @Column (name = "idversion")
    private String idVersion;

    @Column (name = "isversionactive")
    private boolean isVersionActive;

    @Column (name = "note")
    private String note;

    @Column (name = "locationid")
    private long locationId;

    @Column (name = "caseindexid")
    private long caseIndexId;

    @Column (name = "recordid")
    private long recordId;

    @Column (name = "destructionactid")
    private long destructionActId;

    @Column (name = "companyunitid")
    private long companyUnitId;

    @Column (name = "caseaddressblockchain")
    private String caseAddressBlockchain;

    @Column (name = "adddateblockchain")
    private long addDateBlockchain;

    @Column (name = "createdtimestamp")
    private long createdTimestamp;

    @Column (name = "createdby")
    private long createdBy;

    @Column (name = "updatedtimestamp")
    private long updatedTimestamp;

    @Column (name = "updatedby")
    private long updatedBy;
}
