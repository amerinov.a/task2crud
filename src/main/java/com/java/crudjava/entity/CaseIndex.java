package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "caseindex")
public class CaseIndex {
    @Id
    @Column(name = "caseindexid")
    private long caseIndexId;

    @Column(name = "caseindex")
    private long caseIndex;

    @Column(name = "titleru")
    private String titleRu;

    @Column(name = "titlekz")
    private String titleKz;

    @Column(name = "titleen")
    private String titleEn;

    @Column(name = "storagetype")
    private Integer storageType;

    @Column(name = "storageyear")
    private Integer storageYear;

    @Column(name = "note")
    private String note;

    @Column(name = "companyunitid")
    private long companyUnitId;

    @Column(name = "nomenclatureid")
    private long nomenclatureId;

    @Column (name = "createdtimestamp")
    private long createdTimestamp;

    @Column (name = "createdby")
    private long createdBy;

    @Column (name = "updatedtimestamp")
    private long updatedTimestamp;

    @Column (name = "updatedby")
    private long updatedBy;
}
