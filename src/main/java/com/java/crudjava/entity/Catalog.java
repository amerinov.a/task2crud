package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "catalog")
public class Catalog {
    @Id
    @Column (name = "catalogid")
    private long catalogId;

    @Column (name = "nameru")
    private String nameRu;

    @Column (name = "namekz")
    private String nameKz;

    @Column (name = "nameen")
    private String nameEn;

    @Column (name = "parentid")
    private long parentId;

    @Column (name = "companyunitid")
    private long companyUnitId;

    @Column (name = "createdtimestamp")
    private long createdTimestamp;

    @Column (name = "createdby")
    private long createdBy;

    @Column (name = "updatedtimestamp")
    private long updatedTimestamp;

    @Column (name = "updatedby")
    private long updatedBy;
}
