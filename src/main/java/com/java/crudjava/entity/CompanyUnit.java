package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "companyunit")
public class CompanyUnit {
    @Id
    @Column (name = "companyunitid")
    private long companyUnitId;

    @Column(name = "nameru")
    private String nameRu;

    @Column(name = "namekz")
    private String nameKz;

    @Column(name = "nameen")
    private String nameEn;

    @Column(name = "parentid")
    private long parentId;

    @Column(name = "year")
    private Integer year;

    @Column(name = "companyid")
    private long companyId;

    @Column(name = "codeindex")
    private String codeIndex;

    @Column(name = "createdtimestamp")
    private long createdTimestamp;

    @Column (name = "createdby")
    private long createdBy;

    @Column (name = "updatedtimestamp")
    private long updatedTimestamp;

    @Column (name = "updatedby")
    private long updatedBy;
}
