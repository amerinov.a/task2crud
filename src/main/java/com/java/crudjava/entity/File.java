package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "file")
public class File {
    @Id
    @Column (name = "fileid")
    private long fileId;

    @Column (name = "filename")
    private String fileName;

    @Column (name = "filetype")
    private String fileType;

    @Column (name = "size")
    private long size;

    @Column (name = "pagecount")
    private Integer pageCount;

    @Column (name = "hash")
    private String hash;

    @Column (name = "isdeleted")
    private boolean isDeleted;

    @Column (name = "filebinaryid")
    private long fileBinaryId;

    @Column (name = "createdtimestamp")
    private long createdTimestamp;

    @Column (name = "createdby")
    private long createdBy;

    @Column (name = "updatedtimestamp")
    private long updatedTimestamp;

    @Column (name = "updatedby")
    private long updatedBy;
}
