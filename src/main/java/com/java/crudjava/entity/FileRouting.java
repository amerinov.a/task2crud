package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "filerouting")
public class FileRouting {
    @Id
    @Column(name = "fileroutingid")
    private long fileRoutingId;

    @Column(name = "fileid")
    private long fileId;

    @Column(name = "tablename")
    private String tableName;

    @Column(name = "tableid")
    private long tableId;

    @Column (name = "filetype")
    private String fileType;
}

