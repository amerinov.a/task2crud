package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "location")
public class Location {
    @Id
    @Column(name = "locationid")
    private long locationId;

    @Column(name = "row")
    private String row;

    @Column(name = "line")
    private String line;

    @Column(name = "columnn")
    private String column;

    @Column(name = "box")
    private String box;

    @Column(name = "companyunitid")
    private long companyUnitId;

    @Column(name = "createdtimestamp")
    private long createdTimestamp;

    @Column (name = "createdby")
    private long createdBy;

    @Column (name = "updatedtimestamp")
    private long updatedTimestamp;

    @Column (name = "updatedby")
    private long updatedBy;
}
