package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "nomenclature")
public class Nomenclature {
    @Id
    @Column(name = "nomenclatureid")
    private long nomenclatureId;

    @Column(name = "nomenclaturenumber")
    private String nomenclatureNumber;

    @Column(name = "year")
    private Integer year;

    @Column(name = "nomenclaturesummaryid")
    private long nomenclatureSummaryId;

    @Column(name = "companyunitid")
    private long companyUnitId;

    @Column(name = "createdtimestamp")
    private long createdTimestamp;

    @Column (name = "createdby")
    private long createdBy;

    @Column (name = "updatedtimestamp")
    private long updatedTimestamp;

    @Column (name = "updatedby")
    private long updatedBy;
}
