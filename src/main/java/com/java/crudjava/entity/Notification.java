package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "notification")
public class Notification {
    @Id
    @Column (name = "notificationid")
    private long notificationId;

    @Column (name = "objecttype")
    private String objectType;

    @Column (name = "objectid")
    private long objectId;

    @Column (name = "companyunitid")
    private long companyUnitId;

    @Column (name = "userid")
    private long userId;

    @Column (name = "createdtimestamp")
    private long createdTimestamp;

    @Column (name = "viewedtimestamp")
    private long viewedTimestamp;

    @Column (name = "isviewed")
    private boolean isViewed;

    @Column (name = "title")
    private String title;

    @Column (name = "text")
    private String text;

    @Column (name = "companyid")
    private long companyId;
}
