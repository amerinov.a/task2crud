package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "record")
public class Record {
    @Id
    @Column (name = "recordid")
    private long recordId;

    @Column (name = "recordnumber")
    private String recordNumber;

    @Column (name = "recordtype")
    private String recordType;

    @Column (name = "companyunitid")
    private long companyUnitId;

    @Column (name = "createdtimestamp")
    private long createdTimestamp;

    @Column (name = "createdby")
    private long createdBy;

    @Column (name = "updatedtimestamp")
    private long updatedTimestamp;

    @Column (name = "updatedby")
    private long updatedBy;
}




