package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "request")
public class Request {
    @Id
    @Column(name = "requestid")
    private long requestId;

    @Column(name = "requestuserid")
    private long requestUserId;

    @Column(name = "responseuserid")
    private long responseUserId;

    @Column(name = "caseid")
    private long caseId;

    @Column(name = "caseindexid")
    private long caseIndexId;

    @Column(name = "createdtype")
    private String createdType;

    @Column(name = "comment")
    private String comment;

    @Column (name = "status")
    private String status;

    @Column (name = "createdtimestamp")
    private long createdTimestamp;

    @Column (name = "sharestart")
    private long shareStart;

    @Column (name = "sharefinish")
    private long shareFinish;

    @Column (name = "favorite")
    private boolean favorite;

    @Column (name = "updatedtimestamp")
    private long updatedTimestamp;

    @Column (name = "updatedby")
    private long updatedBy;

    @Column (name = "declinenote")
    private String declineNote;

    @Column (name = "companyunitid")
    private long companyUnitId;

    @Column (name = "fromrequestid")
    private long fromRequestId;
}
