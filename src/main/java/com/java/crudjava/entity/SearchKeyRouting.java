package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "searchkeyrouting")
public class SearchKeyRouting {
    @Id
    @Column (name = "searchkeyroutingid")
    private long searchKeyRoutingId;

    @Column (name = "searchkeyid")
    private long searchKeyId;

    @Column (name = "tablename")
    private String tableName;

    @Column (name = "tableid")
    private long tableId;

    @Column (name = "searchkeyroutingtype")
    private String searchKeyRoutingType;
}
