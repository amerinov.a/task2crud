package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "share")
public class Share {
    @Id
    @Column (name = "shareid")
    private long shareId;

    @Column (name = "requestid")
    private long requestId;

    @Column (name = "note")
    private String note;

    @Column (name = "senderid")
    private long senderId;

    @Column (name = "receiverid")
    private long receiverId;

    @Column (name = "sharetimestamp")
    private long shareTimestamp;
}
