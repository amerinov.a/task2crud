package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "tempfiles")
public class Tempfiles {
    @Id
    @Column (name = "tempfilesid")
    private long tempfilesId;

    @Column (name = "filebinary")
    private String fileBinary;

    @Column (name = "filebinarybyte")
    private byte fileBinaryByte;
}
