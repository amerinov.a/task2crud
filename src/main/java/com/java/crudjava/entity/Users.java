package com.java.crudjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Users {
    @Id
    @Column(name = "usersid")
    private long usersId;

    @Column (name = "authorizationid")
    private long authorizationId;

    @Column (name = "name")
    private String name;

    @Column (name = "surname")
    private String surname;

    @Column (name = "secondname")
    private String secondname;

    @Column (name = "status")
    private String status;

    @Column (name = "companyunitid")
    private long companyUnitId;

    @Column (name = "password")
    private String password;

    @Column (name = "lastlogintimestamp")
    private long lastLoginTimestamp;

    @Column (name = "iin")
    private String iin;

    @Column (name = "isactive")
    private boolean isActive;

    @Column (name = "isactivated")
    private boolean isActivated;

    @Column (name = "createdtimestamp")
    private long createdTimestamp;

    @Column (name = "createdby")
    private long createdBy;

    @Column (name = "updatedtimestamp")
    private long updatedTimestamp;

    @Column (name = "updatedby")
    private long updatedBy;
}
