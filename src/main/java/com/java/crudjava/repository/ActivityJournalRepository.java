package com.java.crudjava.repository;

import com.java.crudjava.entity.ActivityJournal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityJournalRepository extends CrudRepository<ActivityJournal,Long> {
}
