package com.java.crudjava.repository;

import com.java.crudjava.entity.Authorization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AuthorizationRepository extends CrudRepository<Authorization,Long> {
}
