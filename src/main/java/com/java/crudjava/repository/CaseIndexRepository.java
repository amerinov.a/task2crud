package com.java.crudjava.repository;

import com.java.crudjava.entity.CaseIndex;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseIndexRepository extends CrudRepository<CaseIndex,Long> {
}
