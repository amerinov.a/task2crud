package com.java.crudjava.repository;

import com.java.crudjava.entity.Case;
import org.springframework.data.repository.CrudRepository;

public interface CaseRepository extends CrudRepository<Case,Long> {
}
