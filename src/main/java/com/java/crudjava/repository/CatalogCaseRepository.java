package com.java.crudjava.repository;

import com.java.crudjava.entity.CatalogCase;
import org.springframework.data.repository.CrudRepository;

public interface CatalogCaseRepository extends CrudRepository<CatalogCase,Long> {
}
