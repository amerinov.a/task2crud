package com.java.crudjava.repository;

import com.java.crudjava.entity.Catalog;
import org.springframework.data.repository.CrudRepository;

public interface CatalogRepository extends CrudRepository<Catalog,Long> {
}
