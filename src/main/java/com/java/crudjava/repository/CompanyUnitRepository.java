package com.java.crudjava.repository;

import com.java.crudjava.entity.CompanyUnit;
import org.springframework.data.repository.CrudRepository;

public interface CompanyUnitRepository extends CrudRepository<CompanyUnit,Long> {
}
