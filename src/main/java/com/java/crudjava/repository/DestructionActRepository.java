package com.java.crudjava.repository;

import com.java.crudjava.entity.DestructionAct;
import org.springframework.data.repository.CrudRepository;

public interface DestructionActRepository extends CrudRepository<DestructionAct,Long> {
}