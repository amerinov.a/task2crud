package com.java.crudjava.repository;

import com.java.crudjava.entity.File;
import org.springframework.data.repository.CrudRepository;

public interface FileRepository extends CrudRepository<File,Long> {
}
