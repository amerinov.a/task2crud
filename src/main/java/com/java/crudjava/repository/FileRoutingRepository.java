package com.java.crudjava.repository;

import com.java.crudjava.entity.FileRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface FileRoutingRepository extends CrudRepository<FileRouting,Long> {
}