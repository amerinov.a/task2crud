package com.java.crudjava.repository;

import com.java.crudjava.entity.Fond;
import org.springframework.data.repository.CrudRepository;

public interface FondRepository extends CrudRepository<Fond,Long> {
}
