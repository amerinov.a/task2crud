package com.java.crudjava.repository;

import com.java.crudjava.entity.Location;
import org.springframework.data.repository.CrudRepository;

public interface LocationRepository extends CrudRepository<Location,Long> {
}
