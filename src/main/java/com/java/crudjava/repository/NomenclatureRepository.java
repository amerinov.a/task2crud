package com.java.crudjava.repository;

import com.java.crudjava.entity.Nomenclature;
import org.springframework.data.repository.CrudRepository;

public interface NomenclatureRepository extends CrudRepository<Nomenclature,Long> {
}
