package com.java.crudjava.repository;

import com.java.crudjava.entity.NomenclatureSummary;
import org.springframework.data.repository.CrudRepository;

public interface NomenclatureSummaryRepository extends CrudRepository<NomenclatureSummary,Long> {
}
