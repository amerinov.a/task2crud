package com.java.crudjava.repository;

import com.java.crudjava.entity.Request;
import org.springframework.data.repository.CrudRepository;

public interface RequestRepository extends CrudRepository<Request,Long> {
}
