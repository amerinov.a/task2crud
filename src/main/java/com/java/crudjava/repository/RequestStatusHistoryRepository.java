package com.java.crudjava.repository;

import com.java.crudjava.entity.RequestStatusHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestStatusHistoryRepository extends CrudRepository<RequestStatusHistory,Long>{
}
