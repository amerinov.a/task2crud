package com.java.crudjava.repository;

import com.java.crudjava.entity.SearchKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRepository extends CrudRepository<SearchKey,Long> {
}
