package com.java.crudjava.repository;

import com.java.crudjava.entity.SearchKeyRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRoutingRepository extends CrudRepository<SearchKeyRouting,Long> {
}
