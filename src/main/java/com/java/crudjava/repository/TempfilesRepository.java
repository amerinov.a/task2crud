package com.java.crudjava.repository;

import com.java.crudjava.entity.Tempfiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TempfilesRepository extends CrudRepository<Tempfiles, Long> {
}