package com.java.crudjava.service;

import com.java.crudjava.entity.CatalogCase;
import com.java.crudjava.repository.CatalogCaseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogCaseService {
    public final CatalogCaseRepository catalogCaseRepository;

    public CatalogCaseService(CatalogCaseRepository catalogCaseRepository) {
        this.catalogCaseRepository = catalogCaseRepository;
    }

    public List<CatalogCase> getAll(){
        return (List<CatalogCase>) catalogCaseRepository.findAll();
    }

    public CatalogCase getById(Long catalogCaseId) {
        return catalogCaseRepository.findById(catalogCaseId).orElse(null);
    }

    public CatalogCase create(CatalogCase catalogCase) {
        return catalogCaseRepository.save(catalogCase);
    }

    public CatalogCase update(CatalogCase catalogCase) {
        return catalogCaseRepository.save(catalogCase);
    }

    public void delete(Long catalogCaseId) {
        catalogCaseRepository.deleteById(catalogCaseId);
    }
}
