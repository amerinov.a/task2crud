package com.java.crudjava.service;

import com.java.crudjava.entity.Catalog;
import com.java.crudjava.repository.CatalogRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogService {
    public final CatalogRepository catalogRepository;

    public CatalogService(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    public List<Catalog> getAll(){
        return (List<Catalog>) catalogRepository.findAll();
    }

    public Catalog getById(Long catalogId) {
        return catalogRepository.findById(catalogId).orElse(null);
    }

    public Catalog create(Catalog catalog) {
        return catalogRepository.save(catalog);
    }

    public Catalog update(Catalog catalog) {
        return catalogRepository.save(catalog);
    }

    public void delete(Long catalogId) {
        catalogRepository.deleteById(catalogId);
    }
}
