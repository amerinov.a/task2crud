package com.java.crudjava.service;

import com.java.crudjava.entity.Company;
import com.java.crudjava.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    public final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getAll(){
        return (List<Company>) companyRepository.findAll();
    }

    public Company getById(Long companyId) {
        return companyRepository.findById(companyId).orElse(null);
    }

    public Company create(Company company) {
        return companyRepository.save(company);
    }

    public Company update(Company company) {
        return companyRepository.save(company);
    }

    public void delete(Long companyId) {
        companyRepository.deleteById(companyId);
    }
}
