package com.java.crudjava.service;

import com.java.crudjava.entity.DestructionAct;
import com.java.crudjava.repository.DestructionActRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DestructionActService {
    public final DestructionActRepository destructionActRepository;

    public DestructionActService(DestructionActRepository destructionActRepository) {
        this.destructionActRepository = destructionActRepository;
    }

    public List<DestructionAct> getAll(){
        return (List<DestructionAct>) destructionActRepository.findAll();
    }

    public DestructionAct getById(Long destructionActId) {
        return destructionActRepository.findById(destructionActId).orElse(null);
    }

    public DestructionAct create(DestructionAct destructionAct) {
        return destructionActRepository.save(destructionAct);
    }

    public DestructionAct update(DestructionAct destructionAct) {
        return destructionActRepository.save(destructionAct);
    }

    public void delete(Long destructionActId) {
        destructionActRepository.deleteById(destructionActId);
    }

}
