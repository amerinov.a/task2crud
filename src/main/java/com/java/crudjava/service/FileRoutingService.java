package com.java.crudjava.service;

import com.java.crudjava.entity.FileRouting;
import com.java.crudjava.repository.FileRoutingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileRoutingService {
    public final FileRoutingRepository fileRoutingRepository;

    public FileRoutingService(FileRoutingRepository fileRoutingRepository) {
        this.fileRoutingRepository = fileRoutingRepository;
    }

    public List<FileRouting> getAll(){
        return (List<FileRouting>) fileRoutingRepository.findAll();
    }

    public FileRouting getById(Long fileRoutingId) {
        return fileRoutingRepository.findById(fileRoutingId).orElse(null);
    }

    public FileRouting create(FileRouting fileRouting) {
        return fileRoutingRepository.save(fileRouting);
    }

    public FileRouting update(FileRouting fileRouting) {
        return fileRoutingRepository.save(fileRouting);
    }

    public void delete(Long fileRoutingId) {
        fileRoutingRepository.deleteById(fileRoutingId);
    }
}
