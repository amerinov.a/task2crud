package com.java.crudjava.service;

import com.java.crudjava.entity.File;
import com.java.crudjava.repository.FileRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileService {
    public final FileRepository fileRepository;

    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public List<File> getAll(){
        return (List<File>) fileRepository.findAll();
    }

    public File getById(Long fileId) {
        return fileRepository.findById(fileId).orElse(null);
    }

    public File create(File file) {
        return fileRepository.save(file);
    }

    public File update(File file) {
        return fileRepository.save(file);
    }

    public void delete(Long fileId) {
        fileRepository.deleteById(fileId);
    }
}
