package com.java.crudjava.service;

import com.java.crudjava.entity.Fond;
import com.java.crudjava.repository.FondRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FondService {
    public final FondRepository fondRepository;

    public FondService(FondRepository fondRepository) {
        this.fondRepository = fondRepository;
    }

    public List<Fond> getAll(){
        return (List<Fond>) fondRepository.findAll();
    }

    public Fond getById(Long fondId) {
        return fondRepository.findById(fondId).orElse(null);
    }

    public Fond create(Fond fond) {
        return fondRepository.save(fond);
    }

    public Fond update(Fond fond) {
        return fondRepository.save(fond);
    }

    public void delete(Long fondId) {
        fondRepository.deleteById(fondId);
    }
}
