package com.java.crudjava.service;

import com.java.crudjava.entity.Nomenclature;
import com.java.crudjava.repository.NomenclatureRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NomenclatureService {
    public final NomenclatureRepository nomenclatureRepository;

    public NomenclatureService(NomenclatureRepository nomenclatureRepository) {
        this.nomenclatureRepository = nomenclatureRepository;
    }

    public List<Nomenclature> getAll(){
        return (List<Nomenclature>) nomenclatureRepository.findAll();
    }

    public Nomenclature getById(Long nomenclatureId) {
        return nomenclatureRepository.findById(nomenclatureId).orElse(null);
    }

    public Nomenclature create(Nomenclature nomenclature) {
        return nomenclatureRepository.save(nomenclature);
    }

    public Nomenclature update(Nomenclature nomenclature) {
        return nomenclatureRepository.save(nomenclature);
    }

    public void delete(Long nomenclatureId) {
        nomenclatureRepository.deleteById(nomenclatureId);
    }
}
