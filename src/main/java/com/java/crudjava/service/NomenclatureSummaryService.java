package com.java.crudjava.service;

import com.java.crudjava.entity.NomenclatureSummary;
import com.java.crudjava.repository.NomenclatureSummaryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NomenclatureSummaryService {
    public final NomenclatureSummaryRepository nomenclatureSummaryRepository;

    public NomenclatureSummaryService(NomenclatureSummaryRepository nomenclatureSummaryRepository) {
        this.nomenclatureSummaryRepository = nomenclatureSummaryRepository;
    }

    public List<NomenclatureSummary> getAll(){
        return (List<NomenclatureSummary>) nomenclatureSummaryRepository.findAll();
    }

    public NomenclatureSummary getById(Long nomenclatureSummaryId) {
        return nomenclatureSummaryRepository.findById(nomenclatureSummaryId).orElse(null);
    }

    public NomenclatureSummary create(NomenclatureSummary nomenclatureSummary) {
        return nomenclatureSummaryRepository.save(nomenclatureSummary);
    }

    public NomenclatureSummary update(NomenclatureSummary nomenclatureSummary) {
        return nomenclatureSummaryRepository.save(nomenclatureSummary);
    }

    public void delete(Long nomenclatureSummaryId) {
        nomenclatureSummaryRepository.deleteById(nomenclatureSummaryId);
    }
}
