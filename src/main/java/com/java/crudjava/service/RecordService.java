package com.java.crudjava.service;

import com.java.crudjava.entity.Record;
import com.java.crudjava.repository.RecordRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecordService {
    public final RecordRepository recordRepository;

    public RecordService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    public List<Record> getAll(){
        return (List<Record>) recordRepository.findAll();
    }

    public Record getById(Long recordId) {
        return recordRepository.findById(recordId).orElse(null);
    }

    public Record create(Record record){
        return recordRepository.save(record);
    }

    public Record update(Record record) {
        return recordRepository.save(record);
    }

    public void delete(Long recordId) {
        recordRepository.deleteById(recordId);
    }
}
