package com.java.crudjava.service;

import com.java.crudjava.entity.Request;
import com.java.crudjava.repository.RequestRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestService {
    public final RequestRepository requestRepository;

    public RequestService(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    public List<Request> getAll(){
        return (List<Request>) requestRepository.findAll();
    }

    public Request getById(Long requestId) {
        return requestRepository.findById(requestId).orElse(null);
    }

    public Request create(Request request) {
        return requestRepository.save(request);
    }

    public Request update(Request request) {
        return requestRepository.save(request);
    }

    public void delete(Long requestId) {
        requestRepository.deleteById(requestId);
    }
}
