package com.java.crudjava.service;

import com.java.crudjava.entity.SearchKeyRouting;
import com.java.crudjava.repository.SearchKeyRoutingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchKeyRoutingService {
    public final SearchKeyRoutingRepository searchKeyRoutingRepository;

    public SearchKeyRoutingService(SearchKeyRoutingRepository searchKeyRoutingRepository) {
        this.searchKeyRoutingRepository = searchKeyRoutingRepository;
    }

    public List<SearchKeyRouting> getAll() {
        return (List<SearchKeyRouting>) searchKeyRoutingRepository.findAll();
    }

    public SearchKeyRouting getById(Long searchKeyRoutingId) {
        return searchKeyRoutingRepository.findById(searchKeyRoutingId).orElse(null);
    }

    public SearchKeyRouting create(SearchKeyRouting searchKeyRouting) {
        return searchKeyRoutingRepository.save(searchKeyRouting);
    }

    public SearchKeyRouting update(SearchKeyRouting searchKeyRouting) {
        return searchKeyRoutingRepository.save(searchKeyRouting);
    }

    public void delete(Long searchKeyRoutingId) {
        searchKeyRoutingRepository.deleteById(searchKeyRoutingId);
    }
}
