package com.java.crudjava.service;

import com.java.crudjava.entity.SearchKey;
import com.java.crudjava.repository.SearchKeyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchKeyService {
    public final SearchKeyRepository searchKeyRepository;

    public SearchKeyService(SearchKeyRepository searchKeyRepository) {
        this.searchKeyRepository = searchKeyRepository;
    }

    public List<SearchKey> getAll(){
        return (List<SearchKey>) searchKeyRepository.findAll();
    }

    public SearchKey getById(Long searchKeyId) {
        return searchKeyRepository.findById(searchKeyId).orElse(null);
    }

    public SearchKey create(SearchKey searchKey) {
        return searchKeyRepository.save(searchKey);
    }

    public SearchKey update(SearchKey searchKey) {
        return searchKeyRepository.save(searchKey);
    }

    public void delete(Long searchKeyId) {
        searchKeyRepository.deleteById(searchKeyId);
    }
}
