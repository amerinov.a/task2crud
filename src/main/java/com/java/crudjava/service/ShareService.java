package com.java.crudjava.service;

import com.java.crudjava.entity.Share;
import com.java.crudjava.repository.ShareRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShareService {
    public final ShareRepository shareRepository;

    public ShareService(ShareRepository shareRepository) {
        this.shareRepository = shareRepository;
    }

    public List<Share> getAll(){
        return (List<Share>) shareRepository.findAll();
    }

    public Share getById(Long shareId) {
        return shareRepository.findById(shareId).orElse(null);
    }

    public Share create(Share share) {
        return shareRepository.save(share);
    }

    public Share update(Share share) {
        return shareRepository.save(share);
    }

    public void delete(Long shareId) {
        shareRepository.deleteById(shareId);
    }
}
