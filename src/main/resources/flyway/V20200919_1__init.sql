CREATE TABLE if not exists "auth"
(
    authorizationId            serial primary key,
    username                   varchar(256),
    email                      varchar(256),
    password                   varchar(256),
    role                       varchar(256),
    forgotPasswordKey          varchar(256),
    forgotPasswordKeyTimestamp bigint,
    companyUnitId              bigint
);

create table if not exists "users"
(
    usersId            serial primary key,
    authorizationId    bigint,
    name               varchar(256),
    surname            varchar(256),
    secondname         varchar(256),
    status             varchar(256),
    companyUnitId      bigint,
    password           varchar(256),
    lastLoginTimestamp bigint,
    iin                varchar(12),
    isActive           boolean,
    isActivated        boolean,
    createdTimestamp   bigint,
    createdBy          bigint,
    updatedTimestamp   bigint,
    updatedBy          bigint
);


