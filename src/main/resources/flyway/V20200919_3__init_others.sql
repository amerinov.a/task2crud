CREATE TABLE if not exists "actjournal"
(
    activityJournalId serial primary key,
    activityType      varchar(128),
    objectType        varchar(255),
    objectId          bigint,
    createdTimestamp  bigint,
    createdBy         bigint,
    messageLevel      varchar(128),
    message           varchar(255)
);

CREATE TABLE if not exists "case"
(
    caseId                serial primary key,
    caseNumber            varchar(128),
    volumeNumber          varchar(128),
    caseRu                varchar(128),
    caseKz                varchar(128),
    caseEn                varchar(128),
    startDate             bigint,
    finishDate            bigint,
    pageCount             bigint,
    isEcpSignature        boolean,
    ecpSignature          varchar(255),
    isSentNaf             boolean,
    isDeleted             boolean,
    isAccessRestricted    boolean,
    hash                  varchar(128),
    version               integer,
    idVersion             varchar(128),
    isVersionActive       boolean,
    note                  varchar(255),
    locationId            bigint,
    caseIndexId           bigint,
    recordId              bigint,
    destructionActId      bigint,
    companyUnitId         bigint,
    caseAddressBlockchain varchar(128),
    addDateBlockchain     bigint,
    createdTimestamp      bigint,
    createdBy             bigint,
    updatedTimestamp      bigint,
    updatedBy             bigint
);


CREATE TABLE if not exists "fond"
(
    fondId           serial primary key,
    fondNumber       varchar(128),
    createdTimestamp bigint,
    createdBy        bigint,
    updatedTimestamp bigint,
    updatedBy        bigint
);

CREATE TABLE if not exists "tempfiles"
(
    tempfilesId    serial primary key,
    fileBinary     varchar(255),
    fileBinaryByte bytea
);

CREATE TABLE if not exists "filerouting"
(
    fileRoutingId serial primary key,
    fileId        bigint,
    tableName     varchar(128),
    tableId       bigint,
    fileType      varchar(128)
);

CREATE TABLE if not exists "file"
(
    fileId           serial primary key,
    fileName         varchar(128),
    fileType         varchar(128),
    size             bigint,
    pageCount        integer,
    hash             varchar(128),
    isDeleted        boolean,
    fileBinaryId     bigint,
    createdTimestamp bigint,
    createdBy        bigint,
    updatedTimestamp bigint,
    updatedBy        bigint
);
CREATE TABLE if not exists "caseindex"
(
    caseIndexId      serial primary key,
    caseIndex        varchar(128),
    titleRu          varchar(128),
    titleKz          varchar(128),
    titleEn          varchar(128),
    storageType      integer,
    storageYear      integer,
    note             varchar(128),
    companyUnitId    bigint,
    nomenclatureId   bigint,
    createdTimestamp bigint,
    createdBy        bigint,
    updatedTimestamp bigint,
    updatedBy        bigint
);

CREATE TABLE if not exists "destructionact"
(
    destructionActId serial primary key,
    numberAct        varchar(128),
    base             varchar(256),
    companyUnitId    bigint,
    createdTimestamp bigint,
    createdBy        bigint,
    updatedTimestamp bigint,
    updatedBy        bigint
);
CREATE TABLE if not exists "searchkeyrouting"
(
    searchKeyRoutingId   serial primary key,
    searchKeyId          bigint,
    tableName            varchar(128),
    tableId              bigint,
    searchKeyRoutingType varchar(128)
);

CREATE TABLE if not exists "share"
(
    shareId        serial primary key,
    requestId      bigint,
    note           varchar(255),
    senderId       bigint,
    receiverId     bigint,
    shareTimestamp bigint
);

CREATE TABLE if not exists "catalog"
(
    catalogId        serial primary key,
    nameRu           varchar(128),
    nameKz           varchar(128),
    nameEn           varchar(128),
    parentId         bigint,
    companyUnitId    bigint,
    createdTimestamp bigint,
    createdBy        bigint,
    updatedTimestamp bigint,
    updatedBy        bigint

);



CREATE TABLE if not exists "reqstatushist"
(
    requestStatusHistoryId serial primary key,
    requestId              bigint,
    status                 varchar(128),
    createdTimestamp       bigint,
    createdBy              bigint,
    updatedTimestamp       bigint,
    updatedBy              bigint

);

CREATE TABLE if not exists "catalogcase"
(
    catalogCaseId    serial primary key,
    caseId           bigint,
    catalogId        bigint,
    companyUnitId    bigint,
    createdTimestamp bigint,
    createdBy        bigint,
    updatedTimestamp bigint,
    updatedBy        bigint

);

CREATE TABLE if not exists "nomenclaturesum"
(
    nomenclatureSummaryId     serial primary key,
    nomenclatureSummaryNumber varchar(128),
    year                      integer,
    companyUnitId             bigint,
    createdTimestamp          bigint,
    createdBy                 bigint,
    updatedTimestamp          bigint,
    updatedBy                 bigint
);

CREATE TABLE if not exists "searchkey"
(
    searchKeyId      serial primary key,
    searchKeyName    varchar(128),
    companyUnitId    bigint,
    createdTimestamp bigint,
    createdBy        bigint,
    updatedTimestamp bigint,
    updatedBy        bigint

);
CREATE TABLE if not exists "record"
(
    recordId         serial primary key,
    recordNumber     varchar(128),
    recordType       varchar(128),
    companyUnitId    bigint,
    createdTimestamp bigint,
    createdBy        bigint,
    updatedTimestamp bigint,
    updatedBy        bigint

);
CREATE TABLE if not exists "company"
(
    companyId        serial primary key,
    nameRu           varchar(128),
    nameKz           varchar(128),
    nameEn           varchar(128),
    bin              varchar(32),
    parentId         bigint,
    fondId           bigint,
    createdTimestamp bigint,
    createdBy        bigint,
    updatedTimestamp bigint,
    updatedBy        bigint
);

CREATE TABLE if not exists "companyunit"
(
    companyUnitId    serial primary key,
    nameRu           varchar(128),
    nameKz           varchar(128),
    nameEn           varchar(128),
    parentId         bigint,
    year             integer,
    companyId        integer,
    codeIndex        varchar(16),
    createdTimestamp bigint,
    createdBy        bigint,
    updatedTimestamp bigint,
    updatedBy        bigint
);

CREATE TABLE if not exists "notification"
(
    notificationId   serial primary key,
    objectType       varchar(128),
    objectId         bigint,
    companyUnitId    bigint,
    userId           bigint,
    createdTimestamp bigint,
    viewedTimestamp  bigint,
    isViewed         boolean,
    title            varchar(255),
    text             varchar(255),
    companyId        bigint
);

CREATE TABLE if not exists "nomenclature"
(
    nomenclatureId        serial primary key,
    nomenclatureNumber    varchar(128),
    year                  integer,
    nomenclatureSummaryId bigint,
    companyUnitId         bigint,
    createdTimestamp      bigint,
    createdBy             bigint,
    updatedTimestamp      bigint,
    updatedBy             bigint
);

CREATE TABLE if not exists "request"
(
    requestId        serial primary key,
    requestUserId    bigint,
    responseUserId   bigint,
    caseId           bigint,
    caseIndexId      bigint,
    createdType      varchar(64),
    comment          varchar(255),
    status           varchar(64),
    createdTimestamp bigint,
    shareStart       bigint,
    shareFinish      bigint,
    favorite         boolean,
    updatedTimestamp bigint,
    updatedBy        bigint,
    declineNote      varchar(255),
    companyUnitId    bigint,
    fromRequestId    bigint

);

CREATE TABLE if not exists "location"
(
    locationId       serial primary key,
    row              varchar(64),
    line             varchar(64),
    columnn          varchar(64),
    box              varchar(64),
    companyUnitId    bigint,
    createdTimestamp bigint,
    createdBy        bigint,
    updatedTimestamp bigint,
    updatedBy        bigint
);


